#include "Server.h"

#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>
#include <omp.h> 
#include <deque>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <unordered_map>


using namespace std;

deque < pair<string, SOCKET> >  clientsList; //global
deque <string> msgList;

unordered_map<string, vector<string>> results;

mutex mtx;
condition_variable condV;

fstream dataFile;
fstream logFile;

//SOCKET client_socket;

Server::Server()
{
	// notice that we step out to the global namespace
	// for the resolution of the function socket

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

	//ifstream f(PATH1);
	int rc;
	char *zErrMsg = 0;
	rc = sqlite3_open("Users.db", &this->_usersDb);
	
	if (!rc)
	{
		const char* sql = "create table Users(id integer primary key autoincrement, username TEXT,password TEXT,highScore INT);";
		rc = sqlite3_exec(this->_usersDb, sql, NULL, 0, &zErrMsg);

		/*cout << "Can't open database: " << sqlite3_errmsg(this->_usersDb) << endl;
		sqlite3_close(_usersDb);
		system("Pause");
		//return(1);*/
	}


	/*const char* sql = "create table Users("  \
		"id INT primary key autoincrement," \
		"username TEXT,"\
		"password TEXT,"\
		"highScore INT);";*/

	

/*	else
	{
		rc = sqlite3_open("Users.db", &this->_usersDb);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
		}
	}*/

	rc = sqlite3_exec(_usersDb, "insert into Users(username,password,highScore) values('tamar','1234',50)",NULL, 0, &zErrMsg);
	//rc = sqlite3_exec(_usersDb, "insert into Users(password) values('1234')", NULL, 0, &zErrMsg);
	//rc = sqlite3_exec(_usersDb, "select * from Users", NULL, 0, &zErrMsg);

}



Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{

	thread t2(&Server::outMsgDeq, this); //thread to take care of messeges
	t2.detach();



	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

										/*
										// Get the local host information
										localHost = gethostbyname("");
										localIP = inet_ntoa (*(struct in_addr *)*localHost->h_addr_list);
										*/

										// again stepping out to the global namespace
										// Connects between the socket and the configuration (port and etc..)
	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	cout << "binded" << endl;

	// Start listening for incoming requests of client
	/*
	input:
	socket
	backlog:The maximum length of the queue of pending connections.
	*/
	if (::listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	cout << "Listening on port " << port << endl;


	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		cout << "accepting client..." << endl;
		accept();
	}


}


void Server::accept()
{
	// this accepts the client and create a specific socket from server to this client

	//input: socket , sockaddr* addr,int* addrlen
	//	output: socket -- wait(block) until client connected
	//
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	cout << "Client accepted!" << endl;

	thread t1(&Server::clientHandler, this, client_socket); //thread to every client
	t1.detach();

	//clientHandler(client_socket);

	// the function that handle the conversation with the client
	//clientHandler(client_socket);
}

/*
This function will put messeges in the msg line, using conditional variable
INPUT: the socket of the client
OUTPUT: void
*/
void Server::clientHandler(SOCKET clientSocket)
{

	int type = 0;
	int sizeData = 0;
	string dataStr;
	string msg;



	try
	{
		type = Helper::getMessageTypeCode(clientSocket);

		while (type != CLIENT_EXIT && type != 0)
		{

			msg = 
			addMsgToDeq(msg);

			type = Helper::getMessageTypeCode(clientSocket);




			//type = Helper::getMessageTypeCode(clientSocket);

			//string s = "Welcome! What is your name (4 bytes)? ";
			//::send(clientSocket, s.c_str(), s.size(), 0);  // last parameter: flag. for us will be 0.

			/*char m[30];
			::recv(clientSocket, m, 30, 0);
			int type = Helper::getMessageTypeCode(clientSocket);
			string sql = "";
			int rc;
			char *zErrMsg = 0;
			string s = "";*/
			/*if (type == CLIENT_LOG_IN) //200
			{
				/*string username = Helper::getUsername(clientSocket);
				string password = Helper::getPassword(clientSocket);
				sql = "select id from Users where username=" + username + ";";
				rc = sqlite3_exec(this->_usersDb, sql.c_str(), NULL, 0, &zErrMsg);
				if (rc == SQLITE_OK) // User does exist
				{
				sql = "insert into Users(username,password,highScore) values(" + username + "," + password + "," + "0);";
				rc = sqlite3_exec(this->_usersDb, sql.c_str(), NULL, 0, &zErrMsg);
				if (rc == SQLITE_OK)
				{
				s = SERVER_SIGN_IN;
				::send(clientSocket, s.c_str(), s.size(), 0);
				}
				}
				else // User doesn't exist
				{

				}
			}
			else if (type == CLIENT_LOG_OUT) //201
			{

			}
			else if (type == CLIENT_SIGN_UP) //203
			{
				string username = Helper::getUsername(clientSocket);
				string password = Helper::getPassword(clientSocket);

				msg = Helper::getPartFromSocket(clientSocket, 50, 0);

				cout << "the messege: " << msg << endl;

				addMsgToDeq(msg);
				
				/*sql = "select id from Users where username=" + username + ";";
				rc = sqlite3_exec(this->_usersDb, sql.c_str(), NULL, 0, &zErrMsg);
				if (rc != SQLITE_OK) // User doesn't exist yet
				{
					sql = "insert into Users(username,password,highScore) values(" + username + "," + password + "," + "0);";
					rc = sqlite3_exec(this->_usersDb, sql.c_str(), NULL, 0, &zErrMsg);
					if (rc == SQLITE_OK)
					{
						s = SERVER_SIGN_UP;
						::send(clientSocket, s.c_str(), s.size(), 0);
					}
				}
				else // User already exists
				{

				}
			}
			else if (type == CLIENT_LIST_EXIST_ROOMS) //205
			{

			}
			else if (type == CLIENT_LIST_USERS_ROOM) //207
			{

			}
			else if (type == CLIENT_JOIN_EXIST_ROOM) //209
			{

			}
			else if (type == CLIENT_LEAVE_ROOM) //211
			{

			}
			else if (type == CLIENT_CREATE_ROOM) //213
			{

			}
			else if (type == CLIENT_CLOSE_ROOM) //215
			{

			}
			else if (type == CLIENT_START_GAME) //217
			{

			}
			else if (type == CLIENT_RESPONSE) //219
			{

			}
			else if (type == CLIENT_LEAVE_GAME) //222
			{

			}
			else if (type == CLIENT_BEST_SCORES) //223
			{

			}
			else if (type == CLIENT_ACHIEVEMENTS) //225
			{

			}
			else if (type == CLIENT_EXIT) //299
			{

			}

			//m[4] = 0;
			//cout << "Client name is: " << m << endl;

			//s = "Bye";


			// Closing the socket (in the level of the TCP protocol)*/
		}
		
		if (type == CLIENT_EXIT)
		{
			::closesocket(clientSocket);
		}
	}
	
	catch (const std::exception& e)
	{
		::closesocket(clientSocket);
	}


}
/*
This function will take care of the messeges that in line of messeges
INPUT: nun
OUTPUT: void
*/
void Server::outMsgDeq()
{

	int msgType = 0;
	string frontMsg;
	string sub;
	string dataFileStr;
	string fileStr;
	int sizeData = 0;
	string dataStr;
	int counter = 1;


	while (true)
	{
		msgType = 0;
		sizeData = 0;



		std::unique_lock <mutex> locker(mtx);
		condV.wait(locker, []() {return !msgList.empty(); }); //wait until there will be a messeges
		frontMsg = msgList.front();
		sub = frontMsg.substr(0, MSG_TYPE_SIZE);
		msgList.pop_front();
		locker.unlock();
		msgType = atoi(sub.data()); //get the messege type

		string currName = clientsList.front().first;
		SOCKET currSock = clientsList.front().second;
		string currSockStr = std::to_string(currSock);

		if (msgType == CLIENT_SIGN_UP) //203
		{
			cout << "I am here" << endl;
			/*string username =  Helper::getUsername(client_socket);
			string password = Helper::getPassword(client_socket);*/
			/*sql = "select id from Users where username=" + username + ";";
			rc = sqlite3_exec(this->_usersDb, sql.c_str(), NULL, 0, &zErrMsg);
			if (rc != SQLITE_OK) // User doesn't exist yet
			{
				sql = "insert into Users(username,password,highScore) values(" + username + "," + password + "," + "0);";
				rc = sqlite3_exec(this->_usersDb, sql.c_str(), NULL, 0, &zErrMsg);
				if (rc == SQLITE_OK)
				{
					s = SERVER_SIGN_UP;
					::send(client_socket, s.c_str(), s.size(), 0);
				}
			}*/
		}
		//read from file
		/*while (getline(dataFile, dataFileStr))
		{
			fileStr += dataFileStr;
		}*/

		/*if (msgType == CLIENT_SIGN_IN)
		{
			std::deque < pair<string, SOCKET> >::iterator it;
			int dataSize = std::atoi(frontMsg.substr(PLACE_DATA_SIZE, DATA_SIZE).c_str());
			string msgData = frontMsg.substr(CONTENT_SIZE, dataSize);

			for (it = clientsList.begin(); it != clientsList.end(); it++)
			{
				if (it->first == msgData)
				{
					break;
				}
				counter++; //count the pozition
			}

			//send msg to client
			//Helper::sendUpdateMessageToClient(clientsList.front().second, dataFileStr, clientsList.front().first, checkNextUser(), counter);
		}*/
		/*if (msgType == MT_CLIENT_UPDATE)
		{
			string strSize = frontMsg.substr(PLACE_DATA_SIZE, CONTENT_SIZE);
			sizeData = atoi(strSize.data());

			dataStr = frontMsg.substr(CONTENT_PLACE, sizeData);

			dataFile << dataStr; //Update the shared document

								 //send all users
			sendUpdate(clientsList, dataStr);

		}*/
		/*if (msgType == MT_CLIENT_FINISH)
		{
			dataStr = frontMsg.substr(CONTENT_PLACE, frontMsg.size());

			dataFile << dataStr; //Update the shared document

								 //pul out and put in the end of list
			string nameC = clientsList.front().first;
			SOCKET sockC = clientsList.front().second;
			clientsList.pop_front();
			clientsList.push_back(pair<string, SOCKET>(nameC, sockC));

			while (getline(dataFile, dataFileStr))
			{
				fileStr += dataFileStr;
			}

			sendUpdate(clientsList, dataStr);

		}
		*/
		


	}//end while


}

/*
This function will cheak who is the next user in list
INPUT: nun
OUTPUT: the name of next user
*/
/*string Server::checkNextUser()
{
	if (clientsList.size() > 1)
	{
		return clientsList.at(1).first;
	}
	else
	{
		return " ";
	}
}*/


/*
This function send update msg to all users (101)
INPUT: the list, the data of the file
OUTPUT: void
*/
void Server::sendUpdate(std::deque<pair<string, SOCKET>> list, string fileData)
{
	std::deque < std::pair<string, SOCKET> >::iterator item;
	int pos = 1;
	for (item = list.begin(); item != list.end(); item++)
	{
		//Helper::sendUpdateMessageToClient(item->second, fileData, list.front().first, checkNextUser(), pos);

		pos++;
	}
}

int Server::callback(void * notUsed, int argc, char ** argv, char ** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

void Server::addMsgToDeq(string msg)
{
	std::unique_lock <mutex> locker(mtx); //lock the acsses to deque
	msgList.push_back(msg); //put the message
							//condV.notify_one(); //awake the thread that took care of messeges
	locker.unlock(); //relese the lock
	condV.notify_all(); //awake the thread that took care of messeges

}





