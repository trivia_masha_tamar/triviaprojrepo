﻿#pragma once

#include <WinSock2.h>
#include <Windows.h>

#include "Helper.h"
#include "sqlite3.h"
#include <string>
#include <fstream>
#include <iostream>
#include<vector>
#include <thread>
#include <omp.h> 
#include <deque>
#include <chrono>
#include <mutex>
#include <condition_variable>

#define DATA_SIZE 2
#define CONTENT_SIZE 5
#define CONTENT_PLACE 8
#define MSG_TYPE_SIZE 3
#define PLACE_DATA_SIZE 3


#define BUFFER_SIZE 1024

#define ACII_DIFFERENCE 48
#define TEN 10
#define NUM_MSG 2
#define CLIENT_SERVER_ID 200
#define START_DATA 5

//#define us

#define CLIENT_LOG_IN 200
#define CLIENT_LOG_OUT 201
#define CLIENT_SIGN_UP 203
#define CLIENT_LIST_EXIST_ROOMS 205
#define CLIENT_LIST_USERS_ROOM 207
#define CLIENT_JOIN_EXIST_ROOM 209
#define CLIENT_LEAVE_ROOM 211
#define CLIENT_CREATE_ROOM 213
#define CLIENT_CLOSE_ROOM 215
#define CLIENT_START_GAME 217
#define CLIENT_RESPONSE 219
#define CLIENT_LEAVE_GAME 222
#define CLIENT_BEST_SCORES 223
#define CLIENT_ACHIEVEMENTS 225
#define CLIENT_EXIT 299

#define SERVER_SIGN_IN 102
#define SERVER_SIGN_UP 104
#define SERVER_LIST_EXIST_ROOMS 106
#define SERVER_LIST_USERS_ROOM 108
#define SERVER_JOIN_EXIST_ROOM 110
#define SERVER_LEAVE_ROOM 112
#define SERVER_CREATE_ROOM 114
#define SERVER_CLOSE_ROOM 116
#define SERVER_QUESTION_ANSWERS 118
#define SERVER_ANSWER_RIGHT 120
#define SERVER_END_GAME 121
#define SERVER_BEST_SCORES 124
#define SERVER_ACHIEVEMENTS 126

#define PATH1 "C:\\Users\\magshimim\\Desktop\\Trivia\\TriviaServ\\Users.db"
#define PATH2 "C:\\Users\\magshimim\\Desktop\\Trivia\\TriviaServ\\Logged.db"

using namespace std;


class Server
{
public:
	Server();
	~Server();
	void serve(int port);

	void outMsgDeq();
	//string checkNextUser();

	void sendUpdate(std::deque< pair<string, SOCKET> > list, string fileData);

	int callback(void* notUsed, int argc, char** argv, char** azCol);

	void addMsgToDeq(string msg);

private:

	void accept();
	void clientHandler(SOCKET clientSocket); 

	SOCKET _serverSocket;

	sqlite3* _usersDb;
	sqlite3* _loggedUsers; // בסוף תקשורת עם השרת יש למחוק את הטבלה בכדי ליצור אחת חדשה כל פעם מחדש
};

