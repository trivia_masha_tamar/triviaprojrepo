﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaProj
{
    public partial class SignUp : Form
    {
        Client cl;
        const int _size = 5;

        const int CLIENT_LOG_IN = 200;
        const int CLIENT_LOG_OUT = 201;
        const int CLIENT_SIGN_UP = 203;
        const int CLIENT_LIST_EXIST_ROOMS = 205;
        const int CLIENT_LIST_USERS_ROOM = 207;
        const int CLIENT_JOIN_EXIST_ROOM = 209;
        const int CLIENT_LEAVE_ROOM = 211;
        const int CLIENT_CREATE_ROOM = 213;
        const int CLIENT_CLOSE_ROOM = 215;
        const int CLIENT_START_GAME = 217;
        const int CLIENT_RESPONSE = 219;
        const int CLIENT_LEAVE_GAME = 222;
        const int CLIENT_BEST_SCORES = 223;
        const int CLIENT_ACHIEVEMENTS = 225;
        const int CLIENT_EXIT = 299;

        const int SERVER_SIGN_IN = 102;
        const int SERVER_SIGN_UP = 104;
        const int SERVER_LIST_EXIST_ROOMS = 106;
        const int SERVER_LIST_USERS_ROOM = 108;
        const int SERVER_JOIN_EXIST_ROOM = 110;
        const int SERVER_LEAVE_ROOM = 112;
        const int SERVER_CREATE_ROOM = 114;
        const int SERVER_CLOSE_ROOM = 116;
        const int SERVER_QUESTION_ANSWERS = 118;
        const int SERVER_ANSWER_RIGHT = 120;
        const int SERVER_END_GAME = 121;
        const int SERVER_BEST_SCORES = 124;
        const int SERVER_ACHIEVEMENTS = 126;
        public SignUp()
        {
            InitializeComponent();
            cl = new Client();
        }

        private void txtUser_Click(object sender, EventArgs e)
        {

        }

        private void txtConfirm_Click(object sender, EventArgs e)
        {

        }

        private void btnSign_Click(object sender, EventArgs e)
        {/////////////////////////////////////////////
           


            if (txtBxUser.TextLength >= _size)
            {

                if (txtBxPass.Text.Equals(txtBxConf.Text))
                {
                    if (txtBxPass.TextLength >= _size)
                    {
                        string sizeUserStr = "";
                        int sizeUser = txtBxUser.Text.Length;
                        if (sizeUser<10)
                        {
                            sizeUserStr += "0" + sizeUser.ToString();
                        }
                        else
                        {
                            sizeUserStr += sizeUser.ToString();
                        }
                        string sizePassStr = "";
                        int sizePass = txtBxPass.Text.Length;
                        if (sizeUser < 10)
                        {
                            sizePassStr += "0" + sizePass.ToString();
                        }
                        else
                        {
                            sizePassStr += sizePass.ToString();
                        }
                        string msg =CLIENT_SIGN_UP.ToString() + sizeUserStr + txtBxUser.Text + sizePassStr + txtBxPass.Text;

                        cl.sendMsg(msg);
                        
                        

                        
                        Form1 f = new Form1();
                        this.Hide();
                        f.Show();
                    }
                    else
                    {
                        msg.Text = "The password must contain 5 characters at least.";
                    }
                }
                else
                {
                    msg.Text = "Passwords aren't equal.";
                }
            }
            else
            {
                msg.Text = "The username must contain 5 characters at least.";
            }
        }
       /* else
        {
            msg.Text = "The user already exists.";
        }*/

    }
    
}
