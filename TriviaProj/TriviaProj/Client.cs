﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TriviaProj
{
    public class Client
    {
        private static NetworkStream clientStream;


        public void clientFunc()
        {
            try
            {
                // Invoke((MethodInvoker)changeButDisabled);
                TcpClient client = new TcpClient();

                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8826);

                client.Connect(serverEndPoint);

                clientStream = client.GetStream();


               /* byte[] buffer = new byte[4096];
                int bytesRead = clientStream.Read(buffer, 0, 4096);*/

                //get the first massege
                /*string input = Encoding.ASCII.GetString(buffer, 0, bytesRead);
                Console.WriteLine(input);
                clientStream.Flush();*/

                //if this is rellay the first massege
                /*if (input.Equals(Constants.START_GAME))
                {
                    Invoke((MethodInvoker)changeButDisabled);
                    Invoke((MethodInvoker)GenerateCards);
                }*/
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public void sendMsg(string input)
        {
            byte[] buffer = new ASCIIEncoding().GetBytes(input);
            int len = input.Length;
            clientStream.Write(buffer, 0, len);
            //clientStream.Write()
            clientStream.Flush();
        }
    }
}
