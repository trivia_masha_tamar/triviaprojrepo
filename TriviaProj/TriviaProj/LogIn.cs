﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TriviaProj
{
    public partial class LogIn : Form
    {

        public LogIn()
        {


            InitializeComponent();
            btnSign.Hide();
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            //////////////////////////////////////////////////////
            if (true/*_db.IfUserExists(txtBxUser.Text, txtBxPass.Text)*/)///////////////
            {
                Options o = new Options();
                this.Hide();
                o.Show();
            }
            else
            {
                msg.Text = "The user does not exists. You can sign up.";
                btnSign.Show();
            }
        }

        private void btnSign_Click(object sender, EventArgs e)
        {
            SignUp s = new SignUp();
            this.Hide();
            s.Show();
        }
    }
}
