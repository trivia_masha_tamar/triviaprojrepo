﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace TriviaProj
{

    public partial class Form1 : Form
    {
        const int CLIENT_SIGN_IN = 200;
        const int CLIENT_SIGN_OUT = 201;
        const int CLIENT_SIGN_UP = 203;
        const int CLIENT_LIST_EXIST_ROOMS = 205;
        const int CLIENT_LIST_USERS_ROOM = 207;
        const int CLIENT_JOIN_EXIST_ROOM = 209;
        const int CLIENT_LEAVE_ROOM = 211;
        const int CLIENT_CREATE_ROOM = 213;
        const int CLIENT_CLOSE_ROOM = 215;
        const int CLIENT_START_GAME = 217;
        const int CLIENT_RESPONSE = 219;
        const int CLIENT_LEAVE_GAME = 222;
        const int CLIENT_BEST_SCORES = 223;
        const int CLIENT_ACHIEVEMENTS = 225;
        const int CLIENT_EXIT = 299;

        const int SERVER_SIGN_IN = 102;
        const int SERVER_SIGN_UP = 104;
        const int SERVER_LIST_EXIST_ROOMS = 106;
        const int SERVER_LIST_USERS_ROOM = 108;
        const int SERVER_JOIN_EXIST_ROOM = 110;
        const int SERVER_LEAVE_ROOM = 112;
        const int SERVER_CREATE_ROOM = 114;
        const int SERVER_CLOSE_ROOM = 116;
        const int SERVER_QUESTION_ANSWERS = 118;
        const int SERVER_ANSWER_RIGHT = 120;
        const int SERVER_END_GAME = 121;
        const int SERVER_BEST_SCORES = 124;
        const int SERVER_ACHIEVEMENTS = 126;
        Thread myThread;


        public Form1()
        {
            /*if(!File.Exists(path))
            {
                //SQLiteConnection.CreateFile("MyDatabase.sqlite");
            }*/
          

            InitializeComponent();
            Client cl = new Client();
            myThread = new Thread(cl.clientFunc);
            myThread.Start();

        }

        private void btnSign_Click(object sender, EventArgs e)
        {
            //sendMsg(CLIENT_SIGN_IN.ToString());

            SignUp s = new SignUp();
            this.Hide();
            s.Show();
        }

        private void btnLog_Click(object sender, EventArgs e)
        {
            LogIn l = new LogIn();
            this.Hide();
            l.Show();
        }
    }
}
